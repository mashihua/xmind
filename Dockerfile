FROM node:10.21.0-alpine

LABEL version="1.0"
LABEL description="Demo"

ENV NODE_ENV production

RUN mkdir -p /usr/src/app
COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

WORKDIR /usr/src/app

EXPOSE 8080 4000

VOLUME ["/usr/src/app/data", "/usr/src/app/src"]

CMD [ "npm", "start" ]
