const express = require('express')
const router  = express.Router()

router
  .all('*', (req, res, next) =>{
    next()
  })
  .get('/', (req, res) =>{
    const model = req.app.get('model')
    if(req.query.type){
      return res.json({errno: 0, data: model.categories.filter('type', parseInt(req.query.type))})
    }
    res.json({errno: 0, data: model.categories.all()})
  })
  .get('/:id', (req, res) =>{
    const model = req.app.get('model')
    res.json({errno: 0, data: model.categories.filter('id', req.param('id'))})
  })

module.exports = router
