const express = require('express')
const router  = express.Router()


router
  .all('*', (req, res, next) => {
    next()
  })
  .get('/', (req, res) => {
    const model = req.app.get('model')
    if(req.query.type){
      return res.json({errno: 0, data: model.bill.filter('type', parseInt(req.query.type))})
    }
    res.json({errno: 0, data: model.bill.all()})
  })
  .get('/:start', (req, res) => {
    const model = req.app.get('model')
    res.json({errno: 0, data: model.bill.filterByMonth('time', +req.params.start)})
  })
  .get('/:start/:end', (req, res) => {
    const model = req.app.get('model')
    res.json({errno: 0, data: model.bill.filterByRange('time', [+req.params.start, +req.params.end])})
  })
  .post('/', (req, res) => {
    const model = req.app.get('model')
    req.body.time = req.body.time || Date.now()
    model.bill.add(req.body)
    res.json({errno: 0, data: req.body})
  })

module.exports = router
