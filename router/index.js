const express = require('express')
const router  = express.Router()


router
  .all('*', function(req, res, next){
    next()
  })
  .get('/', function(req, res) {
    res.send('OK')
  })

module.exports = router
