
import moment from 'moment'

const dateFormat = (val) => {
  return moment(val).format('YYYY-MM-DD')
}

const fixed = (val) => {
  return (val / 100).toFixed(2)
}

export { dateFormat, fixed }
