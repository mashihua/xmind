# XMind Demo

本 Demo 项目按照此[需求](https://github.com/xmindltd/hiring/blob/master/frontend-1/README.md)实现，项目目录结构如下。

```
.
├── data
│   ├── bill.csv
│   └── categories.csv
├── lib
│   └── utils.js
├── model
│   ├── index.js
│   ├── loader.js
│   └── model.js
├── public
│   └── index.html
├── router
│   ├── api
│   │   ├── bill.js
│   │   └── category.js
│   └── index.js
├── src
│   ├── components
│   │   ├── bill.vue
│   │   ├── billForm.vue
│   │   └── statistic.vue
│   ├── filters
│   │   └── index.js
│   ├── App.vue
│   └── main.js
├── test
├── views
├── Dockerfile
├── README.md
├── app.js
├── babel.config.js
├── package-lock.json
├── package.json
└── vue.config.js
```

本项目采用 Back-End 加 Front-End 的方式实现。

Back-End 做为数据源提供 API 给 Front-End 并由其最终展示给用户，同时 Front-End 添加记录的数据也是持久存储在 Back-End 的 `csv` 文件里。文件 `model/model.js` 通过一个通用的抽象类为 Back-End 对 `csv` 数据文件提供查找、区间查找和数据持久存储提供支持。

Front-End 采用 `Vue` + `ELement` 做为前端基础来实现界面。体力活，也没啥可描述的。

### 项目依赖安装

本项目依赖 Node 运行环境，其版本最小为 `10.0.0`。见 `package.josn` 的 `engines.node` 配置，安装依赖配置请使用如下命令。

```
npm install
```

### 启动项目

使用如下命令可以启动服务，启动服务后直接打开网页界面。

```
npm start
```

它包含两个命令，使用 `npm run app` 启动 Back-End 服务进程，使用 `npm run serve` 启动 Front-End 的网页界面。


### 已知问题

1. 界面比较简单，不适配移动端，这个阶段没必要花时间做 UI
2. 没有写单元测试，要做的话可以使用 `mocha` 或者 `jasmine`
3. 文档描述账单金额 `amount` 数据类型为 `Float`，货币使用 `Float` 做为数据类型不是好的设计方案，`Integer` 或者 `Decimal` 是比较合适的数据类型

Enjoy it, loves ♥︎ you.