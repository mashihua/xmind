
const { createReadStream }        = require('fs')
const { once }                    = require('events')
const { createInterface }         = require('readline')
const Model                       = require(__dirname + '/model')

const loader = async (file) => {

  let DATA        = []
  let HEADER      = []
  
  const rs = createReadStream(file)
  const rl = createInterface({
    input: rs,
  })

  rl.on('line', (line) => {
    if(HEADER.length == 0){
      HEADER = line.split(',')
    }else{
      const colmuns = line.split(',')
      const item = {}
      HEADER.forEach((key, idx)=>{
        item[key] = type(colmuns[idx])
      })
      DATA.push(item)
    }
  })

  await once(rl, 'close')

  return new Model(DATA, HEADER, file)
}

const type = (val) => {
  let v = parseInt(val)
  // string
  if(isNaN(v) || !/^[-]*\d+$/.test(val)){
    return val
  }
  // Date
  if(/^\d{13,}$/.test(val)){
    return new Date(parseInt(val))
  }
  // int
  // do not store money as float, can be int or Decimal
  return v
}


module.exports = loader