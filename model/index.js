const fs                          = require('fs')
const path                        = require('path')

const loader                      = require(__dirname + '/loader')

const exp                         = module.exports

const FILES   = fs.readdirSync(path.resolve(__dirname, '..', 'data'))
                  .filter((name)=> !name.startsWith('.') && name.endsWith('.csv') )

FILES.forEach(async (file) => {
  const name = file.replace(/\.[^.]+$/, '')
  exp[name] = await loader(path.resolve(__dirname, '..', 'data', file))
})

