const { appendFile }        = require('fs')

class Model {
  constructor (items, header, file) {
    this.items = items
    this.header = header
    this.file = file
  }
  all () {
    return this.items
  }
  join(key, foreignKey, data){
    
  }
  add (item) {
    this.items.push(item)
    const vals = []
    this.header.forEach((key)=>{
      vals.push(item[key])
    })
    appendFile(this.file, '\n' + vals.join(','), (err) => {})
  }
  filter (key, val) {
    let vals = []
    this.items.forEach((item) => {
      if(item[key] == val){
        vals.push(item)
      }
    })
    return vals
  }
  filterByRange (key, vals) {
    let items = []
    this.items.forEach((item) => {
      let val = item[key]
      // existed keys
      if(val != undefined){
        if (val >= vals[0] && val < vals[1]){
          items.push(item)
        }
      }
    })
    return items
  }
  filterByMonth (key, date) {
    const d = new Date(date)
    // the first day of month
    const start = new Date(d.getFullYear(), d.getMonth(), 1)
    // max month is 11
    const next = d.getMonth() == 11
    // the next day of month
    const end = new Date(next ? d.getFullYear() + 1 : d.getFullYear(), next ? 0 : d.getMonth() + 1, 1)
    return this.filterByRange(key, [start, end])
  }
}

module.exports = Model