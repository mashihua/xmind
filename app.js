
const path                = require('path')
const express             = require('express')
const bodyParser          = require('body-parser')

const utils               = require(__dirname + '/lib/utils')
const model               = require(__dirname + '/model')
const router                = path.join(__dirname, 'router')
const app                 = express()

app.set('base', __dirname + '/')
app.set('port', process.env.PORT || 4000) 
app.set('model', model)
app.set('trust proxy', 'loopback')
app.set('view engine', 'pug')
app.set('views', __dirname + '/views')

app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json({ limit: '1mb' }))
app.use(bodyParser.urlencoded({ extended: true }))


utils.files(router, /\.js$/, function(file){
  let module = require(file)
  let path = file.replace(router, '').replace(/\.[^.]+$/, '').replace(/index$/,'')
  // not route
	if(!module.dispatch && !module.stack){
		return
  }
	app.use(path, module)
})

app.use((req, res, next) =>{
  res.status(404).json({ errno: 999, error: 'Not found' })
})

app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  if(app.get('env') === 'development'){
    return res.send(err.stack || err.toString())
  }
  res.json({ errno: err.status, error: 'Server error.' })
})

if (!module.parent) {
  app.listen(app.get('port'), () => {
    console.log('Express server listening on port ' + app.get('port'))
  })
}else{
  module.exports = app
}