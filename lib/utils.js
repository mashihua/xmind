
const fs           = require('fs')

function files(dir,  test , callback){
	if(!/\/$/.test(dir)){
		dir += '/'
  }
  if(!test){
    test = /\.js$/
  }
  let items = fs.readdirSync(dir)
	// find .init.js or init.coffee we place it to first element
	let index = items.findIndex(function(item){return /\/index\.[^/.\\]+$/.test(item)})
	if(index > 0){
		let ele = items.splice(index,1)
		items.splice(0,0,ele[0])
	}
  items.forEach(function(name, i){
		let stat = fs.statSync(dir + name)
    if(stat.isFile() && test.test(name)){
			callback && callback(dir + name)
    }
		if(stat.isDirectory()){
			files(dir + name, test, callback)
		}
  })
}


module.exports = {
  files
}