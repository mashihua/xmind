const path = require('path')
const version = require('./package.json').version
const IS_PROD = ["production", "prod"].includes(process.env.NODE_ENV)

process.env.VUE_APP_VERSION = version

module.exports = {
  productionSourceMap: false,
  filenameHashing: false,
  pages:{
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      version: version,
      title: 'XMind Demo',
      hash: true
    },
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:4000',
        changeOrigin: true,
        onProxyRes: function(proxyRes, req, res) {
          console.log(proxyRes.statusCode, req.path)
        }
      }
    },
  }
}